package org.net4care.xdsconnector;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.net4care.xdsconnector.Utilities.CodedValue;
import org.net4care.xdsconnector.Utilities.MtomMessageCallback;
import org.net4care.xdsconnector.Utilities.PrepareRequestException;
import org.net4care.xdsconnector.Utilities.ProvideAndRegisterDocumentSetRequestTypeBuilder;
import org.net4care.xdsconnector.Utilities.RetrieveDocumentSetRequestTypeBuilder;
import org.net4care.xdsconnector.Utilities.SubmitObjectsRequestHelper;
import org.net4care.xdsconnector.service.ExtrinsicObjectType;
import org.net4care.xdsconnector.service.IdentifiableType;
import org.net4care.xdsconnector.service.ProvideAndRegisterDocumentSetRequestType;
import org.net4care.xdsconnector.service.RegistryResponseType;
import org.net4care.xdsconnector.service.RetrieveDocumentSetRequestType;
import org.net4care.xdsconnector.service.RetrieveDocumentSetResponseType;
import org.net4care.xdsconnector.service.SubmitObjectsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.w3c.dom.Document;

@Configuration
@PropertySource(value = "classpath:xds.properties")
public class RepositoryConnector extends WebServiceGatewaySupport implements IRepositoryConnector {

  @Value("${xds.repositoryUrl}")
  private String repositoryUrl;

  @Value("${xds.repositoryId}")
  private String repositoryId;

  @Value("${xds.homeCommunityId}")
  private String homeCommunityId;

  private static JAXBContext jaxbContext = createJAXBContext();

  private static JAXBContext createJAXBContext() {
    try {
      return JAXBContext.newInstance(Document.class);
    } catch (JAXBException jx) {
      return null;
    }
  }

  @Autowired
  private ProvideAndRegisterDocumentSetRequestTypeBuilder provideAndRegisterBuilder;
  
  @Bean
  public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    return new PropertySourcesPlaceholderConfigurer();
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.net4care.xdsconnector.RepositoryConnnector#retrieveDocumentSet(java.
   * lang.String)
   */

  @Override
  public RetrieveDocumentSetResponseType retrieveDocumentSet(String docId) throws SendRequestException {
    JAXBElement<RetrieveDocumentSetResponseType> result = sendRetrieveDocumentSetsRequest(
      createDocumentRequests(Arrays.asList(docId)));
    return result.getValue();
  }

  @Override
  public RetrieveDocumentSetResponseType retrieveDocumentSet(List<String> docIds) throws SendRequestException {
    JAXBElement<RetrieveDocumentSetResponseType> result = sendRetrieveDocumentSetsRequest(
        createDocumentRequests(docIds));
    return result.getValue();
  }

  @Override
  public RetrieveDocumentSetResponseType retrieveDocumentSet(RetrieveDocumentSetRequestTypeBuilder builder) throws SendRequestException {
    JAXBElement<RetrieveDocumentSetResponseType> result = sendRetrieveDocumentSetsRequest(builder.getRequestPayload());
    return result.getValue();
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.net4care.xdsconnector.RepositoryConnnector#
   * provideAndRegisterCDADocument(org.w3c.dom.Document,
   * org.net4care.xdsconnector.Utilities.CodedValue,
   * org.net4care.xdsconnector.Utilities.CodedValue)
   */
  @Override
  public RegistryResponseType provideAndRegisterCDADocument(Document cda, CodedValue healthcareFacilityType,
    CodedValue practiceSettingsCode) throws PrepareRequestException, SendRequestException {
    ProvideAndRegisterDocumentSetRequestType request =
        buildProvideAndRegisterCDADocumentRequest(cda, healthcareFacilityType, practiceSettingsCode);
    return sendProvideAndRegisterDocumentSetRequest(
      new ProvideAndRegisterDocumentSetRequestTypeBuilder()
      .setRequest(request)
      .getRequestPayload());
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.net4care.xdsconnector.RepositoryConnnector#
   * provideAndRegisterCDADocument(java.lang.String,
   * org.net4care.xdsconnector.Utilities.CodedValue,
   * org.net4care.xdsconnector.Utilities.CodedValue)
   */
  @Override
  public RegistryResponseType provideAndRegisterCDADocument(
    String cdaDocument,
    CodedValue healthcareFacilityTypeCode,
    CodedValue practiceSettingCode) throws PrepareRequestException, SendRequestException {
    return provideAndRegisterCDADocuments(
      Arrays.asList(cdaDocument),
      healthcareFacilityTypeCode,
      practiceSettingCode);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.net4care.xdsconnector.RepositoryConnnector#
   * provideAndRegisterCDADocuments(java.util.List,
   * org.net4care.xdsconnector.Utilities.CodedValue,
   * org.net4care.xdsconnector.Utilities.CodedValue)
   */
  @Override
  public RegistryResponseType provideAndRegisterCDADocuments(
    List<String> cdaDocumentList,
    CodedValue healthcareFacilityTypeCode,
    CodedValue practiceSettingCode) throws PrepareRequestException, SendRequestException {
    return sendProvideAndRegisterDocumentSetRequest(
      provideAndRegisterBuilder
      .newInstance()
      .setRepositoryId(repositoryId)
      .setHomeCommunityId(homeCommunityId)
      .setSourceId(repositoryId)
      .buildProvideAndRegisterDocumentRequest(cdaDocumentList, healthcareFacilityTypeCode, practiceSettingCode)
      .getRequestPayload());
  }

  public RegistryResponseType sendProvideAndRegisterDocumentSetRequest(
    JAXBElement<ProvideAndRegisterDocumentSetRequestType> requestPayload) throws SendRequestException {
    if (requestPayload == null || requestPayload.getValue() == null) {
      throw new IllegalArgumentException("No request to send");
    }
    try {
      @SuppressWarnings("unchecked")
      JAXBElement<RegistryResponseType> result = (JAXBElement<RegistryResponseType>) getWebServiceTemplate()
          .marshalSendAndReceive(requestPayload,
              new MtomMessageCallback(repositoryUrl, "ProvideAndRegisterDocumentSet-b"));
      return result.getValue();
    } catch (Throwable t) {
      throw new SendRequestException(t);
    }
  }

  protected JAXBElement<RetrieveDocumentSetResponseType> sendRetrieveDocumentSetsRequest(
    JAXBElement<RetrieveDocumentSetRequestType> requestPayload) throws SendRequestException {
    try {
      @SuppressWarnings("unchecked")
      JAXBElement<RetrieveDocumentSetResponseType> result = (JAXBElement<RetrieveDocumentSetResponseType>) getWebServiceTemplate()
      .marshalSendAndReceive(requestPayload, new MtomMessageCallback(repositoryUrl, "RetrieveDocumentSet"));
      return result;
    } catch (Throwable t) {
      throw new SendRequestException(t);
    }
  }

  protected JAXBElement<RetrieveDocumentSetRequestType> createDocumentRequests(List<String> docIds) {
    // using the JAXB Wrapper voids the requirement for a @XMLRootElement
    // annotation on the domain model objects
    RetrieveDocumentSetRequestTypeBuilder builder = new RetrieveDocumentSetRequestTypeBuilder();
    for (String docId : docIds) {
      builder.addDocumentRequest(docId, repositoryId, homeCommunityId);
    }
    return builder.getRequestPayload();
  }

  protected ProvideAndRegisterDocumentSetRequestType buildProvideAndRegisterCDADocumentRequest(Document cdaDocument,
      CodedValue healthcareFacilityType, CodedValue practiceSettingsCode) throws PrepareRequestException {
    ProvideAndRegisterDocumentSetRequestType request = new ProvideAndRegisterDocumentSetRequestType();
    SubmitObjectsRequest submitRequest = new SubmitObjectsRequestHelper(repositoryId, homeCommunityId)
        .buildFromCDA(cdaDocument, healthcareFacilityType, practiceSettingsCode);
    request.setSubmitObjectsRequest(submitRequest);

    ByteArrayOutputStream writer = new ByteArrayOutputStream();
    try {
      Marshaller marshaller = jaxbContext.createMarshaller();
      marshaller.marshal(cdaDocument, writer);
    } catch (JAXBException e) {
      logger.error(e.getMessage(), e);
      throw new PrepareRequestException(e);
    }

    ProvideAndRegisterDocumentSetRequestType.Document document = new ProvideAndRegisterDocumentSetRequestType.Document();
    document.setId(getDocumentId(submitRequest));
    document.setValue(writer.toByteArray());
    request.getDocument().add(document);
    return request;
  }

  /**
   * @deprecated Use {@link ProvideAndRegisterDocumentSetRequestTypeBuilder} instead
   */
  protected ProvideAndRegisterDocumentSetRequestType buildProvideAndRegisterCDADocumentRequest(String cdaString,
      CodedValue healthcareFacilityType, CodedValue practiceSettingsCode) throws PrepareRequestException {
    return provideAndRegisterBuilder
        .newInstance()
        .setRepositoryId(repositoryId)
        .setHomeCommunityId(homeCommunityId)
        .setSourceId(repositoryId)
        .buildProvideAndRegisterDocumentRequest(cdaString, healthcareFacilityType, practiceSettingsCode).getRequest();
  }

  /**
   * @deprecated Use {@link ProvideAndRegisterDocumentSetRequestTypeBuilder} instead
   */
  protected ProvideAndRegisterDocumentSetRequestType buildProvideAndRegisterCDADocumentsRequest(List<String> cdaStrings,
    CodedValue healthcareFacilityType, CodedValue practiceSettingsCode) throws PrepareRequestException {
    return provideAndRegisterBuilder
        .newInstance()
        .setRepositoryId(repositoryId)
        .setHomeCommunityId(homeCommunityId)
        .setSourceId(repositoryId)
        .buildProvideAndRegisterDocumentRequest(cdaStrings, healthcareFacilityType, practiceSettingsCode).getRequest();
  }

  protected static String getDocumentId(SubmitObjectsRequest request) {
    for (JAXBElement<? extends IdentifiableType> identifiable : request.getRegistryObjectList().getIdentifiable()) {
      if (identifiable.getValue() instanceof ExtrinsicObjectType) {
        ExtrinsicObjectType object = (ExtrinsicObjectType) identifiable.getValue();
        return object.getId();
      }
    }
    return null;
  }

}
