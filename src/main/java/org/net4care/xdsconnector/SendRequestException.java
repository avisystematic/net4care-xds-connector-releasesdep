package org.net4care.xdsconnector;

public class SendRequestException extends Exception {

  private static final long serialVersionUID = -6917786958494184900L;

  public SendRequestException(String message) {
    super(message);
  }

  public SendRequestException(Throwable throwable) {
    super(throwable);
  }

  public SendRequestException(String message, Throwable throwable) {
    super(message, throwable);
  }

}
