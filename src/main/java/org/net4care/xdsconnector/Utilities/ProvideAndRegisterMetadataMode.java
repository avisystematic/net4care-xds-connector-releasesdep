package org.net4care.xdsconnector.Utilities;

import java.util.List;

public class ProvideAndRegisterMetadataMode {

  private SourcePatientInfoHandling sourcePatientInfoHandling;
  private DocumentUniqueIdHandling documentUniqueIdHandling;
  
  public ProvideAndRegisterMetadataMode() {
    setSourcePatientInfoHandling(new SourcePatientInfoHandling());
    setDocumentUniqueIdHandling(new TruncatingDocumentUniqueIdHandling());
  }
  
  public static class SourcePatientInfoHandling {
    /**
     * Determines whether to populate the SourcePatientInfo attribute of the DocumentEntry metadata
     * @param patientNames
     * @param patientBirthTime
     * @param patientGender
     * @return true when the SourcePatientInfo attribute should be populated, false otherwise
     */
    public boolean doPopulateSourcePatientInfo(List<String> patientNames, String patientBirthTime, String patientGender) {
      return true;
    }
  }

  public SourcePatientInfoHandling getSourcePatientInfoHandling() {
    return sourcePatientInfoHandling;
  }

  public void setSourcePatientInfoHandling(SourcePatientInfoHandling sourcePatientInfoHandling) {
    this.sourcePatientInfoHandling = sourcePatientInfoHandling;
  }
  
  public DocumentUniqueIdHandling getDocumentUniqueIdHandling() {
    return documentUniqueIdHandling;
  }

  public void setDocumentUniqueIdHandling(DocumentUniqueIdHandling documentUniqueIdHandling) {
    this.documentUniqueIdHandling = documentUniqueIdHandling;
  }

  public static class DocumentUniqueIdHandling {
    public String getRootAndExtensionFormat() {
      return "%s^%s";
    }
    
    public String getDocumentUniqueId(String root, String extension) {
      if (extension != null && !extension.trim().isEmpty()) {
        return String.format(getRootAndExtensionFormat(), root, extension); 
      }
      return root;
    }
  }
  
  public static class TruncatingDocumentUniqueIdHandling extends DocumentUniqueIdHandling {
    @Override
    public String getDocumentUniqueId(String root, String extension) {
      if (extension != null && !extension.trim().isEmpty()) {
        return String.format(getRootAndExtensionFormat(), root, extension.replace("-", "").substring(0, 16));
      } else {
        return root;
      }
    }
  }
  
}
