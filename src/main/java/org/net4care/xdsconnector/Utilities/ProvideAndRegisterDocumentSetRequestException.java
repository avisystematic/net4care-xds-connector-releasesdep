package org.net4care.xdsconnector.Utilities;

public class ProvideAndRegisterDocumentSetRequestException extends PrepareRequestException {

  private static final long serialVersionUID = -1077977709641585044L;

  public ProvideAndRegisterDocumentSetRequestException(String message) {
    super(message);
  }

  public ProvideAndRegisterDocumentSetRequestException(Throwable throwable) {
    super(throwable);
  }

  public ProvideAndRegisterDocumentSetRequestException(String message, Throwable throwable) {
    super(message, throwable);
  }

}
